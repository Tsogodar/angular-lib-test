import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class ConsentsPrinterService {
    constructor(config, http) {
        this.config = config;
        this.http = http;
    }
    fetchConsents(id) {
        return this.http.get(`${this.config.fetchUrl}/${id}`).pipe(
        // tap(consents => console.log(consents)),
        map(consents => {
            return {
                id: consents.data.id,
                current_form_snapshot_id: consents.data.attributes.current_form_snapshot_id,
                included: consents.included
                    .filter(include => include.type === 'consent')
                    .map(consent => {
                    var _a;
                    return {
                        id: consent.id,
                        is_required_by_default: consent.attributes.is_required_by_default,
                        is_checkbox_hidden_by_default: consent.attributes.is_checkbox_hidden_by_default,
                        // is_field_visible_on_form: consent.attributes.is_field_visible_on_form,
                        priority: (_a = consent.attributes.priority) !== null && _a !== void 0 ? _a : 9999,
                        content: consent.attributes.content,
                        lead: consent.attributes.lead
                    };
                }).sort((a, b) => a.priority - b.priority)
            };
        }));
    }
}
ConsentsPrinterService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ConsentsPrinterService_Factory() { return new ConsentsPrinterService(i0.ɵɵinject("forRootConfig"), i0.ɵɵinject(i1.HttpClient)); }, token: ConsentsPrinterService, providedIn: "root" });
ConsentsPrinterService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
ConsentsPrinterService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: ['forRootConfig',] }] },
    { type: HttpClient }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uc2VudHMtcHJpbnRlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vbW9kdWxlcy9jb25zZW50cy1wcmludGVyL3NyYy9saWIvY29uc2VudHMtcHJpbnRlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxNQUFNLEVBQUUsVUFBVSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBRWpELE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUVoRCxPQUFPLEVBQUMsR0FBRyxFQUFNLE1BQU0sZ0JBQWdCLENBQUM7OztBQU14QyxNQUFNLE9BQU8sc0JBQXNCO0lBQ2pDLFlBQ21DLE1BQW1CLEVBQzVDLElBQWdCO1FBRFMsV0FBTSxHQUFOLE1BQU0sQ0FBYTtRQUM1QyxTQUFJLEdBQUosSUFBSSxDQUFZO0lBRTFCLENBQUM7SUFFRCxhQUFhLENBQUMsRUFBbUI7UUFDL0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBWSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSTtRQUNuRSwwQ0FBMEM7UUFDMUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2IsT0FBTztnQkFDTCxFQUFFLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNwQix3QkFBd0IsRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyx3QkFBd0I7Z0JBQzNFLFFBQVEsRUFBRSxRQUFRLENBQUMsUUFBUTtxQkFDeEIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksS0FBSyxTQUFTLENBQUM7cUJBQzdDLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBRTs7b0JBQ2IsT0FBTzt3QkFDTCxFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7d0JBQ2Qsc0JBQXNCLEVBQUUsT0FBTyxDQUFDLFVBQVUsQ0FBQyxzQkFBc0I7d0JBQ2pFLDZCQUE2QixFQUFFLE9BQU8sQ0FBQyxVQUFVLENBQUMsNkJBQTZCO3dCQUMvRSx5RUFBeUU7d0JBQ3pFLFFBQVEsUUFBRSxPQUFPLENBQUMsVUFBVSxDQUFDLFFBQVEsbUNBQUksSUFBSTt3QkFDN0MsT0FBTyxFQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUMsT0FBTzt3QkFDcEMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxVQUFVLENBQUMsSUFBSTtxQkFDNUIsQ0FBQztnQkFDTixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUM7YUFDN0MsQ0FBQztRQUNKLENBQUMsQ0FBQyxDQUNILENBQUM7SUFDSixDQUFDOzs7O1lBakNGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OzRDQUdJLE1BQU0sU0FBQyxlQUFlO1lBVm5CLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdCwgSW5qZWN0YWJsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0lSb290Q29uZmlnfSBmcm9tICcuL2ludGVyZmFjZXMvcm9vdENvbmZpZy5pbnRlcmZhY2UnO1xuaW1wb3J0IHtIdHRwQ2xpZW50fSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHttYXAsIHRhcH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgSUNvbnNlbnRzLCBJQ29uc2VudHNJbmNsdWRlZCB9IGZyb20gJy4vaW50ZXJmYWNlcy9jb25zZW50cy5pbnRlcmZhY2UnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBDb25zZW50c1ByaW50ZXJTZXJ2aWNlIHtcbiAgY29uc3RydWN0b3IoXG4gICAgQEluamVjdCgnZm9yUm9vdENvbmZpZycpIHByaXZhdGUgY29uZmlnOiBJUm9vdENvbmZpZyxcbiAgICBwcml2YXRlIGh0dHA6IEh0dHBDbGllbnRcbiAgKSB7XG4gIH1cblxuICBmZXRjaENvbnNlbnRzKGlkOiBudW1iZXIgfCBzdHJpbmcpOiBPYnNlcnZhYmxlPHtba2V5OiBzdHJpbmddOiBhbnksIGluY2x1ZGVkOiBhbnl9PiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8SUNvbnNlbnRzPihgJHt0aGlzLmNvbmZpZy5mZXRjaFVybH0vJHtpZH1gKS5waXBlKFxuICAgICAgLy8gdGFwKGNvbnNlbnRzID0+IGNvbnNvbGUubG9nKGNvbnNlbnRzKSksXG4gICAgICBtYXAoY29uc2VudHMgPT4ge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGlkOiBjb25zZW50cy5kYXRhLmlkLFxuICAgICAgICAgIGN1cnJlbnRfZm9ybV9zbmFwc2hvdF9pZDogY29uc2VudHMuZGF0YS5hdHRyaWJ1dGVzLmN1cnJlbnRfZm9ybV9zbmFwc2hvdF9pZCxcbiAgICAgICAgICBpbmNsdWRlZDogY29uc2VudHMuaW5jbHVkZWRcbiAgICAgICAgICAgIC5maWx0ZXIoaW5jbHVkZSA9PiBpbmNsdWRlLnR5cGUgPT09ICdjb25zZW50JylcbiAgICAgICAgICAgIC5tYXAoY29uc2VudCA9PiB7XG4gICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgaWQ6IGNvbnNlbnQuaWQsXG4gICAgICAgICAgICAgICAgaXNfcmVxdWlyZWRfYnlfZGVmYXVsdDogY29uc2VudC5hdHRyaWJ1dGVzLmlzX3JlcXVpcmVkX2J5X2RlZmF1bHQsXG4gICAgICAgICAgICAgICAgaXNfY2hlY2tib3hfaGlkZGVuX2J5X2RlZmF1bHQ6IGNvbnNlbnQuYXR0cmlidXRlcy5pc19jaGVja2JveF9oaWRkZW5fYnlfZGVmYXVsdCxcbiAgICAgICAgICAgICAgICAvLyBpc19maWVsZF92aXNpYmxlX29uX2Zvcm06IGNvbnNlbnQuYXR0cmlidXRlcy5pc19maWVsZF92aXNpYmxlX29uX2Zvcm0sXG4gICAgICAgICAgICAgICAgcHJpb3JpdHk6IGNvbnNlbnQuYXR0cmlidXRlcy5wcmlvcml0eSA/PyA5OTk5LFxuICAgICAgICAgICAgICAgIGNvbnRlbnQ6ICBjb25zZW50LmF0dHJpYnV0ZXMuY29udGVudCxcbiAgICAgICAgICAgICAgICBsZWFkOiBjb25zZW50LmF0dHJpYnV0ZXMubGVhZFxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICB9KS5zb3J0KChhLCBiKSA9PiBhLnByaW9yaXR5IC0gYi5wcmlvcml0eSlcbiAgICAgICAgfTtcbiAgICAgIH0pXG4gICAgKTtcbiAgfVxufVxuIl19