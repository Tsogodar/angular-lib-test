import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ConsentsPrinterService } from './consents-printer.service';
export class ConsentsPrinterComponent {
    constructor(consentsService) {
        this.consentsService = consentsService;
        this.consentOutput = new EventEmitter();
        this.selectedConsents = [];
    }
    ngOnInit() {
        this.fetchConsents();
    }
    fetchConsents() {
        this.consentsService.fetchConsents(this.id).subscribe((consents) => {
            this.fetchedConsents = consents['included'];
            this.populateFormArray(consents['included']);
        });
    }
    populateFormArray(consents) {
        this.consentsFormArray = this.consentsFormGroup.get('consents');
        consents.forEach((consent, index) => {
            this.consentsFormArray.push(new FormControl(false, consent.is_required_by_default &&
                !consent.is_checkbox_hidden_by_default
                ? Validators.requiredTrue
                : null));
            this.selectedConsents.push({ [consent.id]: false });
        });
    }
    checkConsent(event, index) {
        const isChecked = event.checked;
        const value = event.source.value;
        const control = this.consentsFormArray.controls[index];
        control.patchValue(isChecked);
        this.selectedConsents[index] = { [value]: isChecked };
        this.consentOutput.next(this.selectedConsents);
    }
    acceptAllConsents(event) {
        this.consentsFormArray.controls.forEach((control) => control.patchValue(event.checked));
        this.selectedConsents.forEach((consent, index) => (this.selectedConsents[index] = {
            [Object.keys(consent)[0]]: event.checked,
        }));
        this.consentOutput.next(this.selectedConsents);
    }
}
ConsentsPrinterComponent.decorators = [
    { type: Component, args: [{
                selector: 'bbp-consents-printer',
                template: "<div *ngIf=\"consentsFormArray && consentsFormArray.length > 0\" [formGroup] = \"consentsFormGroup\" >\n  <div *ngIf=\"consentsFormArray.length > 2\">\n    <mat-checkbox color=\"primary\" (change)=\"acceptAllConsents($event)\">Akceptuj wszystkie zgody wymienione poni\u017Cej</mat-checkbox>\n  </div>\n  <div formArrayName=\"consents\">\n    <div *ngFor=\"let consent of consentsFormArray.controls; index as i\">\n      <mat-checkbox\n        color=\"primary\"\n        [ngClass]=\"{\n          'checkbox-error': consent.invalid && consent.dirty\n        }\"\n        [formControlName]=\"i\"\n        [value]=\"fetchedConsents[i]['id']\"\n        (change)=\"checkConsent($event, i)\">{{fetchedConsents[i]['lead'] ? fetchedConsents[i]['lead'] : fetchedConsents[i]['content']}}\n      </mat-checkbox>\n    </div>\n  </div>\n</div>",
                styles: ["::ng-deep.mat-checkbox-inner-container{margin:4px 8px auto 0!important}::ng-deep .mat-checkbox-label{white-space:normal!important}::ng-deep .mat-checkbox-label a{text-decoration:none;font-weight:600;color:#056ad7!important}.checkbox-error ::ng-deep .mat-checkbox-frame{border-color:red}"]
            },] }
];
ConsentsPrinterComponent.ctorParameters = () => [
    { type: ConsentsPrinterService }
];
ConsentsPrinterComponent.propDecorators = {
    consentsFormGroup: [{ type: Input }],
    id: [{ type: Input }],
    consentOutput: [{ type: Output, args: ['selectedConsent',] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uc2VudHMtcHJpbnRlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9tb2R1bGVzL2NvbnNlbnRzLXByaW50ZXIvc3JjL2xpYi9jb25zZW50cy1wcmludGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBYSxXQUFXLEVBQWEsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFL0UsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFRcEUsTUFBTSxPQUFPLHdCQUF3QjtJQVFuQyxZQUFvQixlQUF1QztRQUF2QyxvQkFBZSxHQUFmLGVBQWUsQ0FBd0I7UUFMaEMsa0JBQWEsR0FBc0MsSUFBSSxZQUFZLEVBQXVCLENBQUM7UUFHdEgscUJBQWdCLEdBQXdCLEVBQUUsQ0FBQztJQUVtQixDQUFDO0lBRS9ELFFBQVE7UUFDTixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVELGFBQWE7UUFDWCxJQUFJLENBQUMsZUFBZSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7WUFDakUsSUFBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDNUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQy9DLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGlCQUFpQixDQUFDLFFBQWE7UUFDN0IsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQ2pELFVBQVUsQ0FDRSxDQUFDO1FBQ2YsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQVksRUFBRSxLQUFhLEVBQUUsRUFBRTtZQUMvQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUN6QixJQUFJLFdBQVcsQ0FDYixLQUFLLEVBQ0wsT0FBTyxDQUFDLHNCQUFzQjtnQkFDOUIsQ0FBQyxPQUFPLENBQUMsNkJBQTZCO2dCQUNwQyxDQUFDLENBQUMsVUFBVSxDQUFDLFlBQVk7Z0JBQ3pCLENBQUMsQ0FBQyxJQUFJLENBQ1QsQ0FDRixDQUFDO1lBQ0YsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsWUFBWSxDQUFDLEtBQXdCLEVBQUUsS0FBYTtRQUNsRCxNQUFNLFNBQVMsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO1FBQ2hDLE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2pDLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdkQsT0FBTyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxFQUFFLFNBQVMsRUFBRSxDQUFDO1FBQ3RELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxLQUF3QjtRQUN4QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQ2xELE9BQU8sQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUNsQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FDM0IsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUUsQ0FDakIsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLEdBQUc7WUFDOUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLE9BQU87U0FDekMsQ0FBQyxDQUNMLENBQUM7UUFDRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUNqRCxDQUFDOzs7WUFoRUYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxzQkFBc0I7Z0JBQ2hDLHcwQkFBc0M7O2FBRXZDOzs7WUFQUSxzQkFBc0I7OztnQ0FTNUIsS0FBSztpQkFDTCxLQUFLOzRCQUNMLE1BQU0sU0FBQyxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtQXJyYXksIEZvcm1Db250cm9sLCBGb3JtR3JvdXAsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBNYXRDaGVja2JveENoYW5nZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2NoZWNrYm94JztcbmltcG9ydCB7IENvbnNlbnRzUHJpbnRlclNlcnZpY2UgfSBmcm9tICcuL2NvbnNlbnRzLXByaW50ZXIuc2VydmljZSc7XG5pbXBvcnQgeyBJU2VsZWN0ZWRDb25zZW50cyB9IGZyb20gJy4vaW50ZXJmYWNlcy9zZWxlY3RlZENvbnNlbnRzLmludGVyZmFjZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2JicC1jb25zZW50cy1wcmludGVyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbnNlbnRzLXByaW50ZXIuaHRtbCcsXG4gIHN0eWxlVXJsczogWydjb25zZW50cy1wcmludGVyLnNjc3MnXSxcbn0pXG5leHBvcnQgY2xhc3MgQ29uc2VudHNQcmludGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgY29uc2VudHNGb3JtR3JvdXA6IEZvcm1Hcm91cDtcbiAgQElucHV0KCkgaWQ6IG51bWJlciB8IHN0cmluZztcbiAgQE91dHB1dCgnc2VsZWN0ZWRDb25zZW50JykgY29uc2VudE91dHB1dDogRXZlbnRFbWl0dGVyPElTZWxlY3RlZENvbnNlbnRzW10+ID0gbmV3IEV2ZW50RW1pdHRlcjxJU2VsZWN0ZWRDb25zZW50c1tdPigpO1xuICBjb25zZW50c0Zvcm1BcnJheTogRm9ybUFycmF5O1xuICBmZXRjaGVkQ29uc2VudHM6IGFueTtcbiAgc2VsZWN0ZWRDb25zZW50czogSVNlbGVjdGVkQ29uc2VudHNbXSA9IFtdO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgY29uc2VudHNTZXJ2aWNlOiBDb25zZW50c1ByaW50ZXJTZXJ2aWNlKSB7fVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuZmV0Y2hDb25zZW50cygpO1xuICB9XG5cbiAgZmV0Y2hDb25zZW50cygpOiB2b2lkIHtcbiAgICB0aGlzLmNvbnNlbnRzU2VydmljZS5mZXRjaENvbnNlbnRzKHRoaXMuaWQpLnN1YnNjcmliZSgoY29uc2VudHMpID0+IHtcbiAgICAgIHRoaXMuZmV0Y2hlZENvbnNlbnRzID0gY29uc2VudHNbJ2luY2x1ZGVkJ107XG4gICAgICB0aGlzLnBvcHVsYXRlRm9ybUFycmF5KGNvbnNlbnRzWydpbmNsdWRlZCddKTtcbiAgICB9KTtcbiAgfVxuXG4gIHBvcHVsYXRlRm9ybUFycmF5KGNvbnNlbnRzOiBhbnkpOiB2b2lkIHtcbiAgICB0aGlzLmNvbnNlbnRzRm9ybUFycmF5ID0gdGhpcy5jb25zZW50c0Zvcm1Hcm91cC5nZXQoXG4gICAgICAnY29uc2VudHMnXG4gICAgKSBhcyBGb3JtQXJyYXk7XG4gICAgY29uc2VudHMuZm9yRWFjaCgoY29uc2VudDogYW55LCBpbmRleDogbnVtYmVyKSA9PiB7XG4gICAgICB0aGlzLmNvbnNlbnRzRm9ybUFycmF5LnB1c2goXG4gICAgICAgIG5ldyBGb3JtQ29udHJvbChcbiAgICAgICAgICBmYWxzZSxcbiAgICAgICAgICBjb25zZW50LmlzX3JlcXVpcmVkX2J5X2RlZmF1bHQgJiZcbiAgICAgICAgICAhY29uc2VudC5pc19jaGVja2JveF9oaWRkZW5fYnlfZGVmYXVsdFxuICAgICAgICAgICAgPyBWYWxpZGF0b3JzLnJlcXVpcmVkVHJ1ZVxuICAgICAgICAgICAgOiBudWxsXG4gICAgICAgIClcbiAgICAgICk7XG4gICAgICB0aGlzLnNlbGVjdGVkQ29uc2VudHMucHVzaCh7IFtjb25zZW50LmlkXTogZmFsc2UgfSk7XG4gICAgfSk7XG4gIH1cblxuICBjaGVja0NvbnNlbnQoZXZlbnQ6IE1hdENoZWNrYm94Q2hhbmdlLCBpbmRleDogbnVtYmVyKTogdm9pZCB7XG4gICAgY29uc3QgaXNDaGVja2VkID0gZXZlbnQuY2hlY2tlZDtcbiAgICBjb25zdCB2YWx1ZSA9IGV2ZW50LnNvdXJjZS52YWx1ZTtcbiAgICBjb25zdCBjb250cm9sID0gdGhpcy5jb25zZW50c0Zvcm1BcnJheS5jb250cm9sc1tpbmRleF07XG4gICAgY29udHJvbC5wYXRjaFZhbHVlKGlzQ2hlY2tlZCk7XG4gICAgdGhpcy5zZWxlY3RlZENvbnNlbnRzW2luZGV4XSA9IHsgW3ZhbHVlXTogaXNDaGVja2VkIH07XG4gICAgdGhpcy5jb25zZW50T3V0cHV0Lm5leHQodGhpcy5zZWxlY3RlZENvbnNlbnRzKTtcbiAgfVxuXG4gIGFjY2VwdEFsbENvbnNlbnRzKGV2ZW50OiBNYXRDaGVja2JveENoYW5nZSk6IHZvaWQge1xuICAgIHRoaXMuY29uc2VudHNGb3JtQXJyYXkuY29udHJvbHMuZm9yRWFjaCgoY29udHJvbCkgPT5cbiAgICAgIGNvbnRyb2wucGF0Y2hWYWx1ZShldmVudC5jaGVja2VkKVxuICAgICk7XG4gICAgdGhpcy5zZWxlY3RlZENvbnNlbnRzLmZvckVhY2goXG4gICAgICAoY29uc2VudCwgaW5kZXgpID0+XG4gICAgICAgICh0aGlzLnNlbGVjdGVkQ29uc2VudHNbaW5kZXhdID0ge1xuICAgICAgICAgIFtPYmplY3Qua2V5cyhjb25zZW50KVswXV06IGV2ZW50LmNoZWNrZWQsXG4gICAgICAgIH0pXG4gICAgKTtcbiAgICB0aGlzLmNvbnNlbnRPdXRwdXQubmV4dCh0aGlzLnNlbGVjdGVkQ29uc2VudHMpO1xuICB9XG59XG4iXX0=