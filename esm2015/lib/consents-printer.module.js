import { NgModule } from '@angular/core';
import { ConsentsPrinterComponent } from './consents-printer.component';
import { HttpClientModule } from '@angular/common/http';
import { ConsentsPrinterService } from './consents-printer.service';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material.module';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
export class ConsentsPrinterModule {
    static forRoot(config) {
        return {
            ngModule: ConsentsPrinterModule,
            providers: [
                { provide: 'forRootConfig', useValue: config },
                ConsentsPrinterService,
            ]
        };
    }
}
ConsentsPrinterModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    BrowserModule,
                    CommonModule,
                    HttpClientModule,
                    ReactiveFormsModule,
                    MaterialModule
                ],
                declarations: [
                    ConsentsPrinterComponent
                ],
                exports: [
                    ConsentsPrinterComponent,
                ],
                bootstrap: [
                    ConsentsPrinterComponent
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uc2VudHMtcHJpbnRlci5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9tb2R1bGVzL2NvbnNlbnRzLXByaW50ZXIvc3JjL2xpYi9jb25zZW50cy1wcmludGVyLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQXNCLFFBQVEsRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUM1RCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUV4RSxPQUFPLEVBQUMsZ0JBQWdCLEVBQUMsTUFBTSxzQkFBc0IsQ0FBQztBQUN0RCxPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSw0QkFBNEIsQ0FBQztBQUNsRSxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLGNBQWMsRUFBQyxNQUFNLG1CQUFtQixDQUFDO0FBQ2pELE9BQU8sRUFBQyxhQUFhLEVBQUMsTUFBTSwyQkFBMkIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQXNCckQsTUFBTSxPQUFPLHFCQUFxQjtJQUN6QixNQUFNLENBQUMsT0FBTyxDQUFDLE1BQW1CO1FBQ3ZDLE9BQU87WUFDTCxRQUFRLEVBQUUscUJBQXFCO1lBQy9CLFNBQVMsRUFBRTtnQkFDVCxFQUFFLE9BQU8sRUFBRSxlQUFlLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBQztnQkFDN0Msc0JBQXNCO2FBQ3ZCO1NBQ0YsQ0FBQztJQUNKLENBQUM7OztZQTNCRixRQUFRLFNBQUM7Z0JBQ1IsT0FBTyxFQUFFO29CQUNQLGFBQWE7b0JBQ2IsWUFBWTtvQkFDWixnQkFBZ0I7b0JBQ2hCLG1CQUFtQjtvQkFDbkIsY0FBYztpQkFDZjtnQkFDRCxZQUFZLEVBQUU7b0JBQ1osd0JBQXdCO2lCQUN6QjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1Asd0JBQXdCO2lCQUN6QjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1Qsd0JBQXdCO2lCQUN6QjthQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb25zZW50c1ByaW50ZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbnNlbnRzLXByaW50ZXIuY29tcG9uZW50JztcbmltcG9ydCB7SVJvb3RDb25maWd9IGZyb20gJy4vaW50ZXJmYWNlcy9yb290Q29uZmlnLmludGVyZmFjZSc7XG5pbXBvcnQge0h0dHBDbGllbnRNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7Q29uc2VudHNQcmludGVyU2VydmljZX0gZnJvbSAnLi9jb25zZW50cy1wcmludGVyLnNlcnZpY2UnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQge01hdGVyaWFsTW9kdWxlfSBmcm9tICcuL21hdGVyaWFsLm1vZHVsZSc7XG5pbXBvcnQge0Jyb3dzZXJNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xuaW1wb3J0IHsgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuXG5cbkBOZ01vZHVsZSh7XG4gIGltcG9ydHM6IFtcbiAgICBCcm93c2VyTW9kdWxlLFxuICAgIENvbW1vbk1vZHVsZSxcbiAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXG4gICAgTWF0ZXJpYWxNb2R1bGVcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgQ29uc2VudHNQcmludGVyQ29tcG9uZW50XG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBDb25zZW50c1ByaW50ZXJDb21wb25lbnQsXG4gIF0sXG4gIGJvb3RzdHJhcDogW1xuICAgIENvbnNlbnRzUHJpbnRlckNvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIENvbnNlbnRzUHJpbnRlck1vZHVsZSB7XG4gIHB1YmxpYyBzdGF0aWMgZm9yUm9vdChjb25maWc6IElSb290Q29uZmlnKTogTW9kdWxlV2l0aFByb3ZpZGVyczxDb25zZW50c1ByaW50ZXJNb2R1bGU+e1xuICAgIHJldHVybiB7XG4gICAgICBuZ01vZHVsZTogQ29uc2VudHNQcmludGVyTW9kdWxlLFxuICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgIHsgcHJvdmlkZTogJ2ZvclJvb3RDb25maWcnLCB1c2VWYWx1ZTogY29uZmlnfSxcbiAgICAgICAgQ29uc2VudHNQcmludGVyU2VydmljZSxcbiAgICAgIF1cbiAgICB9O1xuICB9XG59XG4iXX0=