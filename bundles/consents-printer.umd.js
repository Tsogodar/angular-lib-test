(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common/http'), require('rxjs/operators'), require('@angular/forms'), require('@angular/common'), require('@angular/material/checkbox'), require('@angular/material/button'), require('@angular/material/input'), require('@angular/platform-browser')) :
    typeof define === 'function' && define.amd ? define('consents-printer', ['exports', '@angular/core', '@angular/common/http', 'rxjs/operators', '@angular/forms', '@angular/common', '@angular/material/checkbox', '@angular/material/button', '@angular/material/input', '@angular/platform-browser'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global['consents-printer'] = {}, global.ng.core, global.ng.common.http, global.rxjs.operators, global.ng.forms, global.ng.common, global.ng.material.checkbox, global.ng.material.button, global.ng.material.input, global.ng.platformBrowser));
}(this, (function (exports, i0, i1, operators, forms, common, checkbox, button, input, platformBrowser) { 'use strict';

    var ConsentsPrinterService = /** @class */ (function () {
        function ConsentsPrinterService(config, http) {
            this.config = config;
            this.http = http;
        }
        ConsentsPrinterService.prototype.fetchConsents = function (id) {
            return this.http.get(this.config.fetchUrl + "/" + id).pipe(
            // tap(consents => console.log(consents)),
            operators.map(function (consents) {
                return {
                    id: consents.data.id,
                    current_form_snapshot_id: consents.data.attributes.current_form_snapshot_id,
                    included: consents.included
                        .filter(function (include) { return include.type === 'consent'; })
                        .map(function (consent) {
                        var _a;
                        return {
                            id: consent.id,
                            is_required_by_default: consent.attributes.is_required_by_default,
                            is_checkbox_hidden_by_default: consent.attributes.is_checkbox_hidden_by_default,
                            // is_field_visible_on_form: consent.attributes.is_field_visible_on_form,
                            priority: (_a = consent.attributes.priority) !== null && _a !== void 0 ? _a : 9999,
                            content: consent.attributes.content,
                            lead: consent.attributes.lead
                        };
                    }).sort(function (a, b) { return a.priority - b.priority; })
                };
            }));
        };
        return ConsentsPrinterService;
    }());
    ConsentsPrinterService.ɵprov = i0.ɵɵdefineInjectable({ factory: function ConsentsPrinterService_Factory() { return new ConsentsPrinterService(i0.ɵɵinject("forRootConfig"), i0.ɵɵinject(i1.HttpClient)); }, token: ConsentsPrinterService, providedIn: "root" });
    ConsentsPrinterService.decorators = [
        { type: i0.Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    ConsentsPrinterService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: i0.Inject, args: ['forRootConfig',] }] },
        { type: i1.HttpClient }
    ]; };

    var ConsentsPrinterComponent = /** @class */ (function () {
        function ConsentsPrinterComponent(consentsService) {
            this.consentsService = consentsService;
            this.consentOutput = new i0.EventEmitter();
            this.selectedConsents = [];
        }
        ConsentsPrinterComponent.prototype.ngOnInit = function () {
            this.fetchConsents();
        };
        ConsentsPrinterComponent.prototype.fetchConsents = function () {
            var _this = this;
            this.consentsService.fetchConsents(this.id).subscribe(function (consents) {
                _this.fetchedConsents = consents['included'];
                _this.populateFormArray(consents['included']);
            });
        };
        ConsentsPrinterComponent.prototype.populateFormArray = function (consents) {
            var _this = this;
            this.consentsFormArray = this.consentsFormGroup.get('consents');
            consents.forEach(function (consent, index) {
                var _a;
                _this.consentsFormArray.push(new forms.FormControl(false, consent.is_required_by_default &&
                    !consent.is_checkbox_hidden_by_default
                    ? forms.Validators.requiredTrue
                    : null));
                _this.selectedConsents.push((_a = {}, _a[consent.id] = false, _a));
            });
        };
        ConsentsPrinterComponent.prototype.checkConsent = function (event, index) {
            var _a;
            var isChecked = event.checked;
            var value = event.source.value;
            var control = this.consentsFormArray.controls[index];
            control.patchValue(isChecked);
            this.selectedConsents[index] = (_a = {}, _a[value] = isChecked, _a);
            this.consentOutput.next(this.selectedConsents);
        };
        ConsentsPrinterComponent.prototype.acceptAllConsents = function (event) {
            var _this = this;
            this.consentsFormArray.controls.forEach(function (control) { return control.patchValue(event.checked); });
            this.selectedConsents.forEach(function (consent, index) {
                var _a;
                return (_this.selectedConsents[index] = (_a = {},
                    _a[Object.keys(consent)[0]] = event.checked,
                    _a));
            });
            this.consentOutput.next(this.selectedConsents);
        };
        return ConsentsPrinterComponent;
    }());
    ConsentsPrinterComponent.decorators = [
        { type: i0.Component, args: [{
                    selector: 'bbp-consents-printer',
                    template: "<div *ngIf=\"consentsFormArray && consentsFormArray.length > 0\" [formGroup] = \"consentsFormGroup\" >\n  <div *ngIf=\"consentsFormArray.length > 2\">\n    <mat-checkbox color=\"primary\" (change)=\"acceptAllConsents($event)\">Akceptuj wszystkie zgody wymienione poni\u017Cej</mat-checkbox>\n  </div>\n  <div formArrayName=\"consents\">\n    <div *ngFor=\"let consent of consentsFormArray.controls; index as i\">\n      <mat-checkbox\n        color=\"primary\"\n        [ngClass]=\"{\n          'checkbox-error': consent.invalid && consent.dirty\n        }\"\n        [formControlName]=\"i\"\n        [value]=\"fetchedConsents[i]['id']\"\n        (change)=\"checkConsent($event, i)\">{{fetchedConsents[i]['lead'] ? fetchedConsents[i]['lead'] : fetchedConsents[i]['content']}}\n      </mat-checkbox>\n    </div>\n  </div>\n</div>",
                    styles: ["::ng-deep.mat-checkbox-inner-container{margin:4px 8px auto 0!important}::ng-deep .mat-checkbox-label{white-space:normal!important}::ng-deep .mat-checkbox-label a{text-decoration:none;font-weight:600;color:#056ad7!important}.checkbox-error ::ng-deep .mat-checkbox-frame{border-color:red}"]
                },] }
    ];
    ConsentsPrinterComponent.ctorParameters = function () { return [
        { type: ConsentsPrinterService }
    ]; };
    ConsentsPrinterComponent.propDecorators = {
        consentsFormGroup: [{ type: i0.Input }],
        id: [{ type: i0.Input }],
        consentOutput: [{ type: i0.Output, args: ['selectedConsent',] }]
    };

    var MaterialModule = /** @class */ (function () {
        function MaterialModule() {
        }
        return MaterialModule;
    }());
    MaterialModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        common.CommonModule,
                        checkbox.MatCheckboxModule,
                        button.MatButtonModule,
                        input.MatInputModule,
                    ],
                    exports: [
                        checkbox.MatCheckboxModule,
                        button.MatButtonModule,
                        input.MatInputModule
                    ]
                },] }
    ];

    var ConsentsPrinterModule = /** @class */ (function () {
        function ConsentsPrinterModule() {
        }
        ConsentsPrinterModule.forRoot = function (config) {
            return {
                ngModule: ConsentsPrinterModule,
                providers: [
                    { provide: 'forRootConfig', useValue: config },
                    ConsentsPrinterService,
                ]
            };
        };
        return ConsentsPrinterModule;
    }());
    ConsentsPrinterModule.decorators = [
        { type: i0.NgModule, args: [{
                    imports: [
                        platformBrowser.BrowserModule,
                        common.CommonModule,
                        i1.HttpClientModule,
                        forms.ReactiveFormsModule,
                        MaterialModule
                    ],
                    declarations: [
                        ConsentsPrinterComponent
                    ],
                    exports: [
                        ConsentsPrinterComponent,
                    ],
                    bootstrap: [
                        ConsentsPrinterComponent
                    ]
                },] }
    ];

    /*
     * Public API Surface of consents-printer
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.ConsentsPrinterComponent = ConsentsPrinterComponent;
    exports.ConsentsPrinterModule = ConsentsPrinterModule;
    exports.ConsentsPrinterService = ConsentsPrinterService;
    exports.ɵb = MaterialModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=consents-printer.umd.js.map
