import { EventEmitter, OnInit } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ConsentsPrinterService } from './consents-printer.service';
import { ISelectedConsents } from './interfaces/selectedConsents.interface';
export declare class ConsentsPrinterComponent implements OnInit {
    private consentsService;
    consentsFormGroup: FormGroup;
    id: number | string;
    consentOutput: EventEmitter<ISelectedConsents[]>;
    consentsFormArray: FormArray;
    fetchedConsents: any;
    selectedConsents: ISelectedConsents[];
    constructor(consentsService: ConsentsPrinterService);
    ngOnInit(): void;
    fetchConsents(): void;
    populateFormArray(consents: any): void;
    checkConsent(event: MatCheckboxChange, index: number): void;
    acceptAllConsents(event: MatCheckboxChange): void;
}
