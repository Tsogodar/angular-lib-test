export interface ISelectedConsents {
    [key: number]: boolean;
}
