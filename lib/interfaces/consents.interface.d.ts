export interface IConsents {
    data: IConsentsData;
    included: IConsentsIncluded[];
}
export interface IConsentsData {
    id: number;
    type: string;
    attributes: {
        accepted_at: any;
        accepted_by_id: any;
        created_at: string;
        created_by_id: any;
        css_max_width_px: any;
        current_form_snapshot_id: number;
        message_id: number;
        name: string;
        on_consents_confirm_redirect_to: any;
        site_id: number;
    };
    relationships: {
        current_form_snapshot: {
            data: {
                type: string;
                id: string;
            };
        };
    };
}
export interface IConsentsIncluded {
    id?: number;
    type?: string;
    attributes: {
        accepted_at?: any;
        accepted_by_id?: any;
        created_at?: string;
        created_by_id?: any;
        form_id?: number;
        consent_category?: number;
        consent_category_label?: number;
        content?: string;
        group_number?: number;
        is_archived?: boolean;
        is_checkbox_hidden?: boolean;
        is_checkbox_hidden_by_default?: boolean;
        is_draft?: boolean;
        is_field_visible_on_form?: boolean;
        is_hidden?: boolean;
        is_hidden_by_default?: boolean;
        is_required?: boolean;
        is_required_by_default?: boolean;
        lead?: string;
        meta?: {
            sm_tag: string;
            sm_tag_id: number;
        };
        name?: string;
        priority?: number;
    };
    relationships?: {
        consents: {
            data: {
                type: string;
                id: string;
            }[];
        };
        input_fields?: {
            data: any[];
        };
    };
}
