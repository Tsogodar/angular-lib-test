import { ModuleWithProviders } from '@angular/core';
import { IRootConfig } from './interfaces/rootConfig.interface';
export declare class ConsentsPrinterModule {
    static forRoot(config: IRootConfig): ModuleWithProviders<ConsentsPrinterModule>;
}
