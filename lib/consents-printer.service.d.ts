import { IRootConfig } from './interfaces/rootConfig.interface';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
export declare class ConsentsPrinterService {
    private config;
    private http;
    constructor(config: IRootConfig, http: HttpClient);
    fetchConsents(id: number | string): Observable<{
        [key: string]: any;
        included: any;
    }>;
}
