import { ɵɵdefineInjectable, ɵɵinject, Injectable, Inject, EventEmitter, Component, Input, Output, NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';

class ConsentsPrinterService {
    constructor(config, http) {
        this.config = config;
        this.http = http;
    }
    fetchConsents(id) {
        return this.http.get(`${this.config.fetchUrl}/${id}`).pipe(
        // tap(consents => console.log(consents)),
        map(consents => {
            return {
                id: consents.data.id,
                current_form_snapshot_id: consents.data.attributes.current_form_snapshot_id,
                included: consents.included
                    .filter(include => include.type === 'consent')
                    .map(consent => {
                    var _a;
                    return {
                        id: consent.id,
                        is_required_by_default: consent.attributes.is_required_by_default,
                        is_checkbox_hidden_by_default: consent.attributes.is_checkbox_hidden_by_default,
                        // is_field_visible_on_form: consent.attributes.is_field_visible_on_form,
                        priority: (_a = consent.attributes.priority) !== null && _a !== void 0 ? _a : 9999,
                        content: consent.attributes.content,
                        lead: consent.attributes.lead
                    };
                }).sort((a, b) => a.priority - b.priority)
            };
        }));
    }
}
ConsentsPrinterService.ɵprov = ɵɵdefineInjectable({ factory: function ConsentsPrinterService_Factory() { return new ConsentsPrinterService(ɵɵinject("forRootConfig"), ɵɵinject(HttpClient)); }, token: ConsentsPrinterService, providedIn: "root" });
ConsentsPrinterService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
ConsentsPrinterService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: ['forRootConfig',] }] },
    { type: HttpClient }
];

class ConsentsPrinterComponent {
    constructor(consentsService) {
        this.consentsService = consentsService;
        this.consentOutput = new EventEmitter();
        this.selectedConsents = [];
    }
    ngOnInit() {
        this.fetchConsents();
    }
    fetchConsents() {
        this.consentsService.fetchConsents(this.id).subscribe((consents) => {
            this.fetchedConsents = consents['included'];
            this.populateFormArray(consents['included']);
        });
    }
    populateFormArray(consents) {
        this.consentsFormArray = this.consentsFormGroup.get('consents');
        consents.forEach((consent, index) => {
            this.consentsFormArray.push(new FormControl(false, consent.is_required_by_default &&
                !consent.is_checkbox_hidden_by_default
                ? Validators.requiredTrue
                : null));
            this.selectedConsents.push({ [consent.id]: false });
        });
    }
    checkConsent(event, index) {
        const isChecked = event.checked;
        const value = event.source.value;
        const control = this.consentsFormArray.controls[index];
        control.patchValue(isChecked);
        this.selectedConsents[index] = { [value]: isChecked };
        this.consentOutput.next(this.selectedConsents);
    }
    acceptAllConsents(event) {
        this.consentsFormArray.controls.forEach((control) => control.patchValue(event.checked));
        this.selectedConsents.forEach((consent, index) => (this.selectedConsents[index] = {
            [Object.keys(consent)[0]]: event.checked,
        }));
        this.consentOutput.next(this.selectedConsents);
    }
}
ConsentsPrinterComponent.decorators = [
    { type: Component, args: [{
                selector: 'bbp-consents-printer',
                template: "<div *ngIf=\"consentsFormArray && consentsFormArray.length > 0\" [formGroup] = \"consentsFormGroup\" >\n  <div *ngIf=\"consentsFormArray.length > 2\">\n    <mat-checkbox color=\"primary\" (change)=\"acceptAllConsents($event)\">Akceptuj wszystkie zgody wymienione poni\u017Cej</mat-checkbox>\n  </div>\n  <div formArrayName=\"consents\">\n    <div *ngFor=\"let consent of consentsFormArray.controls; index as i\">\n      <mat-checkbox\n        color=\"primary\"\n        [ngClass]=\"{\n          'checkbox-error': consent.invalid && consent.dirty\n        }\"\n        [formControlName]=\"i\"\n        [value]=\"fetchedConsents[i]['id']\"\n        (change)=\"checkConsent($event, i)\">{{fetchedConsents[i]['lead'] ? fetchedConsents[i]['lead'] : fetchedConsents[i]['content']}}\n      </mat-checkbox>\n    </div>\n  </div>\n</div>",
                styles: ["::ng-deep.mat-checkbox-inner-container{margin:4px 8px auto 0!important}::ng-deep .mat-checkbox-label{white-space:normal!important}::ng-deep .mat-checkbox-label a{text-decoration:none;font-weight:600;color:#056ad7!important}.checkbox-error ::ng-deep .mat-checkbox-frame{border-color:red}"]
            },] }
];
ConsentsPrinterComponent.ctorParameters = () => [
    { type: ConsentsPrinterService }
];
ConsentsPrinterComponent.propDecorators = {
    consentsFormGroup: [{ type: Input }],
    id: [{ type: Input }],
    consentOutput: [{ type: Output, args: ['selectedConsent',] }]
};

class MaterialModule {
}
MaterialModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    MatCheckboxModule,
                    MatButtonModule,
                    MatInputModule,
                ],
                exports: [
                    MatCheckboxModule,
                    MatButtonModule,
                    MatInputModule
                ]
            },] }
];

class ConsentsPrinterModule {
    static forRoot(config) {
        return {
            ngModule: ConsentsPrinterModule,
            providers: [
                { provide: 'forRootConfig', useValue: config },
                ConsentsPrinterService,
            ]
        };
    }
}
ConsentsPrinterModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    BrowserModule,
                    CommonModule,
                    HttpClientModule,
                    ReactiveFormsModule,
                    MaterialModule
                ],
                declarations: [
                    ConsentsPrinterComponent
                ],
                exports: [
                    ConsentsPrinterComponent,
                ],
                bootstrap: [
                    ConsentsPrinterComponent
                ]
            },] }
];

/*
 * Public API Surface of consents-printer
 */

/**
 * Generated bundle index. Do not edit.
 */

export { ConsentsPrinterComponent, ConsentsPrinterModule, ConsentsPrinterService, MaterialModule as ɵb };
//# sourceMappingURL=consents-printer.js.map
